# NOTAS OCJP

##**INTERFACES**:
- Son parecidas a las clases abstractas
- Solo puedo tener metodos abstract

Se usa por que una clases solo puede tener un pare pero puede implementar n interfaces

```
#!java


<modificador> interface NombreDeLaInterface (){
	void met1();
	void met2();	
}
```

Después de la palabra *implements* se pone todas las interfaces que queremos implementar en esta clase

En la ultima clase en la que se esta implementando la interfaz deben estar todos los métodos 

Aquí deben estar implementadas las interfaces, por ejemplo:
```
#!java


public class ManageR extends Employee //puede tener un padre si esta utilizando herencia
	implements Interface1, Interface2 (){ 
        void met1(){
			...
		}
	void met2(){
			...
		}
}
```

		
## **ENCAPSULAMIENTO**
###Relacion

Todas las clases heredan de la clase object

Hay 2 tipos de relación: hemos visto la primera (Herencia)

- **Herencia** responde al termino ES

```
#!java

	class Empleado extends Persona{
		
	}

```

Un empleado es una persona

- **Polimorfismo** responde al termino TIENE

Un empleado tiene una mascota
	
	
```
#!java

class Mascota------
	- 
 {
		...
	}
		
	class Empleado extends Persona{
		Mascota asd;
	}
```


## **SOBRECARGA DE METODOS** ##
- Funciones con el mismo nombre, pero diferente  numero de atributos
Estamos una clase, y los metodos se llaman de l amisma forma

	
```
#!java

public void metodoA(String s){... }
	public void metodoA(){... }
	public void metodoA(int i, String s){... }
	public void metodoA(String s, int i){... }
```
Todas son diferentes pero tienen el mismo nombre--->esto es sobrecarga

## **SOBREESCRITURA** ##

- Un metodo sobreescrito reemplazara a su metodo valido
- Mantener el tipo de return
- Mantener el numero/tipo/orden de parametros

## **CONSTRUCTOR** ##

- Mismo nombre de la Clase
- Se pueden sobrecargar
- No se heredan
- Para llamar a un constructor de la clase

## **ENUMS** ##

Se tiene un claro ejemplo, de como implementar un semaforo antes:

```
#!java
class Semaforo{
        int estado; 1 - 2 - 3
        String color;
        char asd;
        //Se implementaba de esta manera, asignando una letra a un color o una posición
        String obtenerEstado(){
            //muchos if's
            //un Switch con al menos 3 cases
        }
        
        
    }

```

Se crea una nueva clase llamada 'ColorSemaforo', y en el *IntelliJ* se escoge la opción de Enum


```
#!java
public enum ColorSemaforo {
    ROJO(0), AMARILLO(1), VERDE(2);
//Extiende de una clase object
    int codigo;
    ColorSemaforo(int i){
        this.codigo = i;
    }
    //Entonces se añaden numeros 
    ColorSemaforo(){
    }
}

public class Semaforo {
    ColorSemaforo estado;

    public Semaforo(){
        estado=ColorSemaforo.ROJO;

    }

    public void accion(){
        switch (estado){
            case ROJO:
                System.out.println("Parar");
                break;
            case AMARILLO:
                System.out.println("Acelera");
                break;
            case VERDE:
                System.out.println("Acelera mas");
                break;
            default:
                System.out.println("error");
                //De esta manera sabemos a que color exacto vamos a llamar
        }
    }
}

public calss main

```
De esta manera, si alguien viene a usar mi programa, puede darse cuetna que hay 3 estados: rojo, amarillo o verde.


Usando el ENUM, podemos llamarlo de la siguiente manera:



### Cosas importantes 
- Pueden ser declarados en cualquier lugar en donde una clase pueda ser declarada
- No se instancian
- No pueden heredar ni ser heredado
- Para definir un enum, se usa la palabra reservada enum no class