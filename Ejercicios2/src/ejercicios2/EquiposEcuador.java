/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2;

/**
 *
 * @author david
 */
public enum EquiposEcuador {
    ELNACIONAL ("Rojo",13),
    LIGA ("Blanco",10),
    BARCELONA ("Amarillo",15);  
    
    private final String color; 
    private final int campeonatos; 
 
    EquiposEcuador (String color, int campeonatos) { 
        this.color = color;
        this.campeonatos = campeonatos;
    } 
    public String getColor() { 
        return color; 
        }
    public int getCampeonatos() { 
        return campeonatos; 
    }
} 