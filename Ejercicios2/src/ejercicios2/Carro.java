/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2;

/**
 *
 * @author david
 */
public class Carro {
    private String color;
    private String matricula;
    private String gasolina;

    public String getColor() {
        return color;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getGasolina() {
        return gasolina;
    }
    
       public Carro(String color, String matricula, String gasolina) {
        this.color = color;
        this.matricula = matricula;
        this.gasolina = gasolina;
    }
    
    public Carro(){
        color="rojo";
        matricula="67890324423";
        gasolina="diesel";
    }
}
